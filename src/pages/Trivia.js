import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getRandomClues } from '../store/trivia/selector'
import { getAllClues } from '../store/trivia/slice'


function Trivia() {

const dispatch = useDispatch()
const clues = useSelector(getRandomClues)
useEffect(() => {
  dispatch(getAllClues())
}, [])



  return (
    <div>
        <div id="accordion">
            {
                clues.map(clue =>(
        

                <div key={clue.id} className="card">
                    <div className="card-header" id={clue.id}>
                        <h5 className="mb-0">
                            <button className="btn btn-link" data-toggle="collapse" data-target={`#${clue.id}`} aria-expanded="true" aria-controls={clue.id}>
                            {clue.question}
                            </button>
                        </h5>
                    </div>

                    <div id={clue.id} className="collapse show" aria-labelledby={clue.id} data-parent={`#${clue.id}`}>
                        <div className="card-body">
                            {clue.answer}
                        </div>
                    </div>
                </div>
       
                ))
            }

        </div>
    </div>
  )
}

export default Trivia