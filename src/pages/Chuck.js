import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getRandomJokeSelector } from '../store/chuck/selectors';
import { getRandomJoke, getRandomJokeByCategory} from '../store/chuck/slice';



function Chuck() {
const randomJoke = useSelector(getRandomJokeSelector)
const dispatch = useDispatch()
  useEffect(() => {
    dispatch((getRandomJoke()));
  }, [])

const [category, setCategory] = useState("")


  return (
    <div> 

        <div className="getNewJokeByCategory">
            <input placeholder="Enter category..." value={category} type="text" onChange={({target})=>setCategory(target.value)}/>
            <button disabled={!category} onClick={()=>dispatch(getRandomJokeByCategory(category))} className='btn btn-success ml-2'>Get new joke</button>
        </div>


        <div className="card m-2 p-2 random-joke">
        <span><strong>Random Chuck Norris joke:</strong> </span>
        {randomJoke}

        </div>
    </div>
  )
}

export default Chuck