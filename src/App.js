import { Redirect } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import Chuck from './pages/Chuck';
import Trivia from './pages/Trivia';


function App() {

  

  return (
    <div className="App">
        <Router>
            <Navbar/>

            <div className="container">
            <Switch>
                
                <Route path='/' exact>
                    
                </Route>

                <Route path='/chuck' exact>
                  <Chuck/>
                </Route>

                <Route path='/trivia' exact>
                  <Trivia/>
                </Route>

                <Route path='/' redirect exact>
                    <Redirect to="/" />
                </Route>
              
            </Switch>
            </div>
          </Router>

    </div>
  );
}

export default App;
