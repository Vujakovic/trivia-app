import { createSlice } from "@reduxjs/toolkit";

const middlewareActions = {
    getAllClues: () => {},
};


export const triviaSlice = createSlice({
  name: "trivia",
  initialState: {
    randomClues: []
  },
  reducers: {
    setRandomClues: (state,action) => {
        state.randomClues=action.payload
    },
    ...middlewareActions,
  },
});

export const {getAllClues, setRandomClues } = triviaSlice.actions;

export default triviaSlice.reducer; //
