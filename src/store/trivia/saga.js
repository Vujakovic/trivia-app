import {call, put, takeLatest} from "redux-saga/effects"
import triviaService from "../../service/TriviaService";
import { getAllClues, setRandomClues } from "./slice";




function* getClues() {
    const data = yield call(triviaService.getClues)
    console.log(data);
    yield put(setRandomClues(data))
}




export function* watchForSagas(){
    yield takeLatest(getAllClues.type, getClues)
}
