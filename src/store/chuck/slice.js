import { createSlice } from "@reduxjs/toolkit";

const middlewareActions = {
  getRandomJoke: () => {},
  getRandomJokeByCategory: ()=> {},
};


export const chuckSlice = createSlice({
  name: "chuck",
  initialState: {
    randomJoke: ''
  },
  reducers: {
    setRandomJoke: (state, action) => {
        state.randomJoke= action.payload
    },
    ...middlewareActions,
  },
});

export const { setRandomJoke, getRandomJoke, getRandomJokeByCategory} = chuckSlice.actions;

export default chuckSlice.reducer; //
