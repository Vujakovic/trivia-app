import {call, put, takeLatest} from "redux-saga/effects"
import chuckService from "../../service/ChuckService";
import { getRandomJoke, setRandomJoke, getRandomJokeByCategory } from "./slice";




function* randomJoke() {
    const data = yield call(chuckService.getRandomSingleJoke)
    
    yield put(setRandomJoke(data.value))
}

function* randomJokeByCategory(action) {
    const data = yield call(chuckService.getRandomJokeByCategory, action.payload)
    
    yield put(setRandomJoke(data.value))
}


export function* watchForSagas(){
    yield takeLatest(getRandomJoke.type, randomJoke)
    yield takeLatest(getRandomJokeByCategory.type, randomJokeByCategory)
}
