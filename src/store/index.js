import { configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';
import sagas from './rootSaga';
import chuckReducer from './chuck/slice'
import triviaReducer from './trivia/slice'

const sagaMiddleware = createSagaMiddleware();


export default configureStore({
  reducer: {
    chuck: chuckReducer,
    trivia: triviaReducer
  },
  middleware : [
    ...getDefaultMiddleware(
     { thunk: false }
    ),
    sagaMiddleware
 ]

});




for (let saga in sagas) {
  sagaMiddleware.run(sagas[saga]);
}
