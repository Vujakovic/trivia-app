import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
  return (
    <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <Link to="/" className="navbar-brand" >TriviaApp</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarsExample02">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                <Link to="/"className="nav-link">Home <span className="sr-only">(current)</span></Link>
                </li>

                <li className="nav-item active">
                <Link to="/chuck"className="nav-link">Chuck </Link>
                </li>

                <li className="nav-item active">
                <Link to="/trivia"className="nav-link">Trivia </Link>
                </li>
            
            </ul>
        
            </div>
        </nav>
    </div>
  )
}

export default Navbar