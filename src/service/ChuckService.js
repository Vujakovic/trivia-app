import HttpService from "./HttpService";

class ChuckService extends HttpService {

    getRandomSingleJoke = async () =>{
        const { data } = await this.client.get("https://api.chucknorris.io/jokes/random");
        return data;
      }

    getRandomJokeByCategory = async (category) =>{
        const { data } = await this.client.get(`https://api.chucknorris.io/jokes/random?category=${category}`);
        return data;
      }
    
}

const chuckService = new ChuckService();
export default chuckService;
