import HttpService from "./HttpService";

class TriviaService extends HttpService {

    getClues = async () =>{
        const { data } = await this.client.get("http://jservice.io/api/random?count=30");
        return data;
      }
}

const triviaService = new TriviaService();
export default triviaService;
